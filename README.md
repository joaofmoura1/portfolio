# Portfolio

The main objective of this Data Science personal project portfolio is to demonstrate my skills in solving business challenges through my knowledge and tools of Data Science.

![](https://i2.wp.com/www.cienciaedados.com/wp-content/uploads/2019/06/Por-Que-e-Como-Data-Science-e-Mais-do-Que-Apenas-Machine-Learning.jpg?fit=850%2C425&ssl=1)

# João Felipe Moura
###### _Data Scientist_


-  have knowledge in steps of resolution data science problem, from insights with exploratory data analysis to publishing models in productions.
- I developed insight and regression projects.
- My interests include data science, machine learning and business.

***Analytical Tools:***

**Data Collect and Storage:** MySQL.

**Data Processing and Analysis:** Python and Airflow.  _(Libraries: NumPy, Pandas, Plotly and MatplotLib)_

**Development:** Git

**Data Visualization:** PowerBI

###### Links:
[](https://www.linkedin.com/in/joaofmoura)
<a href="https://www.linkedin.com/in/joaofmoura" alt="linkedin" target="_blank">

<img src="https://img.shields.io/badge/LinkedIn-%230077B5.svg?&style=flat-square&logo=linkedin&logoColor=white">

</a>
<a href="mailto:jf_moura@id.uff.br" alt="gmail" target="_blank">

<img src="https://img.shields.io/badge/-Gmail-FF0000?style=flat-square&labelColor=FF0000&logo=gmail&logoColor=white&link=mailto:jf_moura@id.uff.br" />

</a>
